package regress;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.skillbox.users.UsersApplication;
import com.skillbox.users.entity.Gender;
import com.skillbox.users.entity.Photo;
import com.skillbox.users.entity.Subscription;
import com.skillbox.users.entity.User;
import com.skillbox.users.repository.PhotoRepository;
import integ.PostgresContainerWrapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.S3;

@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = UsersApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class RegressUsersServiceTest {

    private static final String MALE_GENDER_DESCRIPTION = "Male";

    @LocalServerPort
    private int port;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private PhotoRepository photoRepository;

    @Container
    private static final PostgreSQLContainer<PostgresContainerWrapper> postgresContainer = new PostgresContainerWrapper();

    @Container
    public static LocalStackContainer localstack = new LocalStackContainer(DockerImageName.parse("localstack/localstack:0.11.3"))
            .withServices(S3);

    AmazonS3 s3 = AmazonS3ClientBuilder
            .standard()
            .withEndpointConfiguration(
                    new AwsClientBuilder.EndpointConfiguration(
                            localstack.getEndpoint().toString(),
                            localstack.getRegion()
                    )
            )
            .withCredentials(
                    new AWSStaticCredentialsProvider(
                            new BasicAWSCredentials(localstack.getAccessKey(), localstack.getSecretKey())
                    )
            )
            .build();

    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);

        registry.add("aws.s3.endpoint", localstack::getEndpoint);
        registry.add("aws.s3.access-key", localstack::getAccessKey);
        registry.add("aws.s3.secret-key", localstack::getSecretKey);
        registry.add("aws.s3.region", localstack::getRegion);
    }

    @BeforeEach
    public void init() throws FileNotFoundException {
        String bucketName = "jpeg";

        if (!s3.doesBucketExistV2(bucketName)) {
            s3.createBucket(bucketName);
        }

        String filePath1 = "src/integTest/resources/images/image1.png";
        File file1 = new File(filePath1);
        ObjectMetadata metadata1 = new ObjectMetadata();
        metadata1.setContentLength(file1.length());
        metadata1.setContentType(String.valueOf(MediaType.IMAGE_JPEG));
        var stream1 = new FileInputStream(file1);
        s3.putObject(bucketName, file1.getName(), stream1, metadata1);

        String filePath2 = "src/integTest/resources/images/image2.png";
        File file2 = new File(filePath2);
        ObjectMetadata metadata2 = new ObjectMetadata();
        metadata2.setContentLength(file2.length());
        metadata2.setContentType(String.valueOf(MediaType.IMAGE_JPEG));
        var stream2 = new FileInputStream(file2);
        s3.putObject(bucketName, file2.getName(), stream2, metadata2);

        String filePath3 = "src/integTest/resources/images/image3.png";
        File file3 = new File(filePath3);
        ObjectMetadata metadata3 = new ObjectMetadata();
        metadata3.setContentLength(file3.length());
        metadata3.setContentType(String.valueOf(MediaType.IMAGE_JPEG));
        var stream3 = new FileInputStream(file3);
        s3.putObject(bucketName, file3.getName(), stream3, metadata3);
    }

    @Test
    @Order(1)
    public void create_two_users_and_get_them_by_id() throws Exception {
        //give
        User user1 = getCommonUser1();
        User user2 = getCommonUser2();

        String filePath1 = "src/integTest/resources/images/image1.png";
        String filePath2 = "src/integTest/resources/images/image2.png";
        Path file1 = Paths.get(filePath1);
        Path file2 = Paths.get(filePath2);
        // Mock files
        MockMultipartFile fileMock1 = new MockMultipartFile("file", "image1.png", "image/png", Files.readAllBytes(file1));
        MockMultipartFile fileMock2 = new MockMultipartFile("file", "image2.png", "image/png", Files.readAllBytes(file2));

        //when then
        mockMvc.perform(MockMvcRequestBuilders.multipart("/user")
                        .file(fileMock1)
                        .flashAttr("user", user1))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(user1.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(user1.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.userNickname").value(user1.getUserNickname()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(user1.getEmail()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.photo").isNotEmpty());

        mockMvc.perform(MockMvcRequestBuilders.get("/user/{id}", 1L))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(user1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(user1.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(user1.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.userNickname").value(user1.getUserNickname()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(user1.getEmail()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.photo").isNotEmpty());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/user")
                        .file(fileMock2)
                        .flashAttr("user", user2))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(user2.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(user2.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.userNickname").value(user2.getUserNickname()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(user2.getEmail()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.photo").isNotEmpty());

        mockMvc.perform(MockMvcRequestBuilders.get("/user/{id}", 2L))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(user2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(user2.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(user2.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.userNickname").value(user2.getUserNickname()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(user2.getEmail()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.photo").isNotEmpty());
    }

    @Test
    @Order(2)
    public void find_all_no_deleted_users() {
        //given
        //when then
        var allUsers = restTemplate.getForEntity(getUserUrl("?isDeleted=false"), List.class);
        assertEquals(200, allUsers.getStatusCodeValue());
        assertEquals(2, Objects.requireNonNull(allUsers.getBody()).size());
    }

    @Test
    @Order(3)
    public void subscribe_user1_to_user2() {
        //given
        //when then
        var getSubscriptionForUser1 = restTemplate.getForEntity(getSubscriptionUrl("1"), List.class);
        assertEquals(200, getSubscriptionForUser1.getStatusCodeValue());
        assertEquals(Collections.emptyList(), getSubscriptionForUser1.getBody());

        var subscribedUser1ToUser2 = restTemplate.postForEntity(getSubscriptionUrl("1/2"), null, Subscription.class);
        assertEquals(200, subscribedUser1ToUser2.getStatusCodeValue());
        assertEquals(new Subscription(1L, 1L, 2L), subscribedUser1ToUser2.getBody());

        var retryGetSubscriptionForUser1 = restTemplate.getForEntity(getSubscriptionUrl("1"), List.class);
        assertEquals(200, retryGetSubscriptionForUser1.getStatusCodeValue());
        assertEquals(2, Objects.requireNonNull(retryGetSubscriptionForUser1.getBody()).get(0));
    }

    @Test
    @Order(4)
    public void delete_subscribe_user1_to_user2() {
        //given
        //when then
        var getSubscriptionForUser1 = restTemplate.getForEntity(getSubscriptionUrl("1"), List.class);
        assertEquals(200, getSubscriptionForUser1.getStatusCodeValue());
        assertEquals(2, Objects.requireNonNull(getSubscriptionForUser1.getBody()).get(0));

        restTemplate.delete(getSubscriptionUrl("1/2"), Void.class);

        var retryGetSubscriptionForUser1 = restTemplate.getForEntity(getSubscriptionUrl("1"), List.class);
        assertEquals(200, retryGetSubscriptionForUser1.getStatusCodeValue());
        assertEquals(Collections.emptyList(), retryGetSubscriptionForUser1.getBody());
    }

    @Test
    @Order(5)
    public void update_user_first_name_and_photo() throws Exception {
        //given
        var newFirstName = "SuperIvan";
        String filePath3 = "src/integTest/resources/images/image3.png";
        Path file3 = Paths.get(filePath3);
        MockMultipartFile fileMock3 = new MockMultipartFile("file", "image3.png", "image/png", Files.readAllBytes(file3));

        //when then
        var gatedUser1 = restTemplate.getForEntity(getUserUrl("1"), User.class);
        var user = Objects.requireNonNull(gatedUser1.getBody());
        assertEquals(200, gatedUser1.getStatusCodeValue());
        assertEquals(getCommonUser1().getFirstName(), user.getFirstName());

        user.setFirstName(newFirstName);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/user/{id}", 1L)
                        .file(fileMock3)
                        .flashAttr("user", user))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(user.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(user.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(user.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.userNickname").value(user.getUserNickname()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(user.getEmail()));

        var retryGatedUser1 = restTemplate.getForEntity(getUserUrl("1"), User.class);
        assertEquals(200, retryGatedUser1.getStatusCodeValue());
        assertEquals(newFirstName, Objects.requireNonNull(retryGatedUser1.getBody()).getFirstName());
    }

    @Test
    @Order(6)
    public void delete_users() {
        //given
        //when then
        var gatedUser1 = restTemplate.getForEntity(getUserUrl("1"), User.class);
        assertEquals(200, gatedUser1.getStatusCodeValue());

        var gatedUser2 = restTemplate.getForEntity(getUserUrl("2"), User.class);
        assertEquals(200, gatedUser2.getStatusCodeValue());

        restTemplate.delete(getUserUrl("1"), Void.class);
        restTemplate.delete(getUserUrl("2"), Void.class);

        var retryGatedUser1 = restTemplate.getForEntity(getUserUrl("1"), User.class);
        assertEquals(500, retryGatedUser1.getStatusCodeValue());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, retryGatedUser1.getStatusCode());

        var retryGatedUser2 = restTemplate.getForEntity(getUserUrl("2"), User.class);
        assertEquals(500, retryGatedUser2.getStatusCodeValue());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, retryGatedUser2.getStatusCode());
    }

    @Test
    @Order(7)
    public void find_all_deleted_users() throws Exception {
        //given
        //when then
        mockMvc.perform(MockMvcRequestBuilders.get("/user?isDeleted=true"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2));
    }


    private User getCommonUser1() {
        User user = new User();
        user.setFirstName("Ivan");
        user.setMiddleName("Ivanovich");
        user.setLastName("Ivanov");
        user.setGender(new Gender(MALE_GENDER_DESCRIPTION));
        user.setDateOfBirth(LocalDate.of(2021, 4, 1));
        user.setCity("Moskva");
        user.setUserNickname("Ivan");
        user.setEmail("Ivan@.com");
        return user;
    }

    private User getCommonUser2() {
        User user = new User();
        user.setFirstName("Petr");
        user.setMiddleName("Petrovich");
        user.setLastName("Petrov");
        user.setGender(new Gender(MALE_GENDER_DESCRIPTION));
        user.setDateOfBirth(LocalDate.of(2023, 5, 21));
        user.setCity("Lokoshko");
        user.setUserNickname("Petr");
        user.setEmail("Petr@.com");
        return user;
    }

    private String getUserUrl(String path) {
        return "http://localhost:" + port + "/user" + "/" + path;
    }

    private String getSubscriptionUrl(String path) {
        return "http://localhost:" + port + "/subscription" + "/" + path;
    }
}
