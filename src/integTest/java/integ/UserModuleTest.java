package integ;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.skillbox.users.UsersApplication;
import com.skillbox.users.entity.Gender;
import com.skillbox.users.entity.User;
import com.skillbox.users.repository.GenderRepository;
import com.skillbox.users.repository.UserRepository;
import com.skillbox.users.service.UsersService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testcontainers.utility.DockerImageName;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.testcontainers.containers.localstack.LocalStackContainer.Service.S3;

@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = UsersApplication.class)
public class UserModuleTest {

    private static final String MALE_GENDER_DESCRIPTION = "Male";
    private static final Gender MALE_GENDER = new Gender(1L, MALE_GENDER_DESCRIPTION);

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UsersService usersService;

    @Autowired
    private GenderRepository genderRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Container
    private static final PostgreSQLContainer<PostgresContainerWrapper> postgresContainer = new PostgresContainerWrapper();

    @Container
    public static LocalStackContainer localstack = new LocalStackContainer(DockerImageName.parse("localstack/localstack:0.11.3"))
            .withServices(S3);

    AmazonS3 s3 = AmazonS3ClientBuilder
            .standard()
            .withEndpointConfiguration(
                    new AwsClientBuilder.EndpointConfiguration(
                            localstack.getEndpoint().toString(),
                            localstack.getRegion()
                    )
            )
            .withCredentials(
                    new AWSStaticCredentialsProvider(
                            new BasicAWSCredentials(localstack.getAccessKey(), localstack.getSecretKey())
                    )
            )
            .build();

    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);

        registry.add("aws.s3.endpoint", localstack::getEndpoint);
        registry.add("aws.s3.access-key", localstack::getAccessKey);
        registry.add("aws.s3.secret-key", localstack::getSecretKey);
        registry.add("aws.s3.region", localstack::getRegion);
    }

    @BeforeEach
    public void cleanDb() throws FileNotFoundException {
        String bucketName = "jpeg";
        String filePath1 = "src/integTest/resources/images/image1.png";


        if (!s3.doesBucketExistV2(bucketName)) {
            s3.createBucket(bucketName);
        }

        File file1 = new File(filePath1);
        ObjectMetadata metadata1 = new ObjectMetadata();
        metadata1.setContentLength(file1.length());
        metadata1.setContentType(String.valueOf(MediaType.IMAGE_JPEG));
        var stream1 = new FileInputStream(file1);
        s3.putObject(bucketName, file1.getName(), stream1, metadata1);
    }

    @Test
    public void saveUser_Success() throws Exception {
        // given
        User userForSave = getCommonUser();
        userForSave.setUserNickname("some");
        userForSave.setEmail("some");

        User savedUser = getCommonUser();
        savedUser.setUserNickname("some");
        savedUser.setEmail("some");

        String filePath1 = "src/integTest/resources/images/image1.png";
        Path file1 = Paths.get(filePath1);
        // Mock files
        MockMultipartFile fileMock1 = new MockMultipartFile("file", "image1.png", "image/png", Files.readAllBytes(file1));

        genderRepository.save(MALE_GENDER);

        // when/then
        mockMvc.perform(MockMvcRequestBuilders.multipart("/user")
                        .file(fileMock1)
                        .flashAttr("user", userForSave))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(savedUser.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(savedUser.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.userNickname").value(savedUser.getUserNickname()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(savedUser.getEmail()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.photo").isNotEmpty());
    }

    @Test
    public void deleteUserById_Success() throws Exception {
        // given
        Long userId = 1L;
        User user = getCommonUser();
        user.setId(userId);
        String filePath1 = "src/integTest/resources/images/image1.png";
        Path file1 = Paths.get(filePath1);
        // Mock files
        MockMultipartFile fileMock1 = new MockMultipartFile("file", "image1.png", "image/png", Files.readAllBytes(file1));

        var savedUser = usersService.saveUser(user, fileMock1);

        // when/then
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/{id}", savedUser.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void getUserById_Success() throws Exception {
        // given
        Long userId = 1L;
        User user = getCommonUser();
        user.setId(userId);
        String filePath1 = "src/integTest/resources/images/image1.png";
        Path file1 = Paths.get(filePath1);
        // Mock files
        MockMultipartFile fileMock1 = new MockMultipartFile("file", "image1.png", "image/png", Files.readAllBytes(file1));

        usersService.saveUser(user, fileMock1);

        // when/then
        mockMvc.perform(MockMvcRequestBuilders.get("/user/{id}", userId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(user.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(user.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(user.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.userNickname").value(user.getUserNickname()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(user.getEmail()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.photo").isNotEmpty());
    }

    @Test
    public void findAllUsers_Deleted_False_Success() throws Exception {
        // given
        User user1 = getCommonUser();
        user1.setId(1L);
        user1.setGender(MALE_GENDER);

        User user2 = getCommonUser();
        user2.setId(2L);
        user2.setGender(MALE_GENDER);
        user2.setUserNickname("someNick");
        user2.setEmail("someEmail");

        List<User> users = List.of(user1, user2);

        genderRepository.save(MALE_GENDER);
        userRepository.saveAll(users);

        // when/then
        mockMvc.perform(MockMvcRequestBuilders.get("/user?isDeleted=false"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(users.size()));
    }

    @Test
    public void updateUserById_Success() throws Exception {
        // given
        Long userId = 2L;
        User savedUser = getCommonUser();
        savedUser.setId(userId);
        savedUser.setUserNickname("Petr");
        savedUser.setEmail("somw@gmai.ru");

        User updatedUser = getCommonUser();
        updatedUser.setId(2L);
        updatedUser.setUserNickname("SuoerNickName");
        updatedUser.setEmail("superEmail");
        String filePath1 = "src/integTest/resources/images/image1.png";
        Path file1 = Paths.get(filePath1);
        // Mock files
        MockMultipartFile fileMock1 = new MockMultipartFile("file", "image1.png", "image/png", Files.readAllBytes(file1));

        usersService.saveUser(savedUser, fileMock1);

        // when/then
        mockMvc.perform(MockMvcRequestBuilders.multipart("/user/{id}", userId)
                        .file(fileMock1)
                        .flashAttr("user", updatedUser))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(updatedUser.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(updatedUser.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(updatedUser.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.userNickname").value(updatedUser.getUserNickname()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value(updatedUser.getEmail()));
    }

    private User getCommonUser() {
        User user = new User();
        user.setFirstName("Ivan");
        user.setLastName("Ivanov");
        user.setGender(new Gender(MALE_GENDER_DESCRIPTION));
        user.setUserNickname("Ivan");
        user.setEmail("Ivan@.com");
        return user;
    }
}
