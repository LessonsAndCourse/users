package com.skillbox.users.unit;

import com.skillbox.users.entity.Gender;
import com.skillbox.users.entity.Photo;
import com.skillbox.users.entity.User;
import com.skillbox.users.exceptions.CommonDataBaseException;
import com.skillbox.users.exceptions.CommonExistException;
import com.skillbox.users.repository.GenderRepository;
import com.skillbox.users.repository.UserRepository;
import com.skillbox.users.service.S3Service;
import com.skillbox.users.service.UsersService;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = UsersService.class)
public class UserServiceTest {

    private static final String MALE_GENDER_DESCRIPTION = "Male";
    private static final Gender MALE_GENDER = new Gender(1L, MALE_GENDER_DESCRIPTION);

    @Autowired
    private UsersService usersService;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private GenderRepository genderRepository;
    @MockBean
    private EntityManager entityManager;
    @Mock
    private Session session;
    @Mock
    private Filter filter;
    @MockBean
    private S3Service s3Service;

    @Test
    public void saveUser_Success() {
        //given
        when(genderRepository.findOrCreateByDescription(anyString()))
                .thenReturn(MALE_GENDER);

        User userFromDb = getCommonUser();
        userFromDb.setId(1L);
        userFromDb.setGender(MALE_GENDER);
        User userForSave = getCommonUser();
        Photo photo = new Photo();
        photo.setName("Photo");
        photo.setLink("url");

        when(userRepository.save(any(User.class)))
                .thenReturn(userFromDb);
        when(s3Service.saveAndGetPhotos(any())).thenReturn(photo);

        //when
        var result = usersService.saveUser(userForSave, null);
        //then
        assertEquals(userFromDb, result);
        verify(genderRepository, times(1)).findOrCreateByDescription(anyString());
        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    void deleteUserById_ShouldThrowException_WhenUserNotFound() {
        //given
        Long userId = 1L;

        when(userRepository.existsById(userId)).thenReturn(false);

        //then
        assertThrows(CommonExistException.class, () -> usersService.deleteUserById(userId));
        verify(userRepository, times(1)).existsById(userId);
        verify(userRepository, never()).deleteById(anyLong());
    }

    @Test
    void getUserById_ShouldReturnUser_WhenUserFound() {
        //given
        Long userId = 1L;

        User userFromDb = getCommonUser();
        userFromDb.setId(userId);
        userFromDb.setGender(MALE_GENDER);

        when(userRepository.findByIdAndDeletedIsFalse(userId))
                .thenReturn(Optional.of(userFromDb));

        //when
        User result = usersService.getUserById(userId);

        //then
        assertEquals(userFromDb, result);
        verify(userRepository, times(1)).findByIdAndDeletedIsFalse(userId);
    }

    @Test
    void getUserById_ShouldThrowException_WhenUserNotFound() {
        //given
        Long userId = 1L;

        when(userRepository.findByIdAndDeletedIsFalse(userId))
                .thenReturn(Optional.empty());

        //then
        assertThrows(CommonExistException.class, () -> usersService.getUserById(userId));
        verify(userRepository, times(1)).findByIdAndDeletedIsFalse(userId);
    }

    @Test
    void updateUserById_ShouldThrowException_WhenUserNotFound() {
        //given
        Long userId = 1L;
        User user = new User();

        when(userRepository.existsById(userId)).thenReturn(false);

        //when
        assertThrows(CommonExistException.class, () -> usersService.updateUserById(userId, user, null));
        verify(userRepository, times(1)).existsById(userId);
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    void findAllUsers_shouldReturnAllUsers_whenIsDeletedIsFalse() {
        //given
        List<User> users = new ArrayList<>();
        users.add(getCommonUser());
        users.add(getCommonUser());
        when(userRepository.findAll()).thenReturn(users);
        when(session.enableFilter(anyString())).thenReturn(filter);
        when(entityManager.unwrap(Session.class)).thenReturn(session);

        //when
        List<User> result = usersService.findAllUsers(false);

        //then
        assertEquals(users, result);
        verify(session, times(1)).enableFilter("deletedUserFilter");
        verify(filter, times(1)).setParameter("isDeleted", false);
        verify(session, times(1)).disableFilter("deletedUserFilter");
        verify(userRepository, times(1)).findAll();
    }

    @Test
    void findAllUsers_shouldReturnDeletedUsers_whenIsDeletedIsTrue() {
        // given
        List<User> deletedUsers = new ArrayList<>();
        deletedUsers.add(getCommonUser());
        deletedUsers.add(getCommonUser());
        when(userRepository.findAll()).thenReturn(deletedUsers);
        when(session.enableFilter(anyString())).thenReturn(filter);
        when(entityManager.unwrap(Session.class)).thenReturn(session);

        // when
        List<User> result = usersService.findAllUsers(true);

        // then
        assertEquals(deletedUsers, result);
        verify(session, times(1)).enableFilter("deletedUserFilter");
        verify(filter, times(1)).setParameter("isDeleted", true);
        verify(session, times(1)).disableFilter("deletedUserFilter");
        verify(userRepository, times(1)).findAll();
    }

    private User getCommonUser() {
        User user = new User();
        user.setFirstName("Ivan");
        user.setLastName("Ivanov");
        user.setGender(new Gender(MALE_GENDER_DESCRIPTION));
        user.setUserNickname("Ivan");
        user.setEmail("Ivan@.com");
        return user;
    }
}
