package com.skillbox.users.service;

import com.skillbox.users.entity.User;
import com.skillbox.users.exceptions.CommonExistException;
import com.skillbox.users.repository.GenderRepository;
import com.skillbox.users.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class UsersService {

    private static final String USER_DOES_NOT_EXIST = "User does not exist";

    private final UserRepository userRepository;
    private final GenderRepository genderRepository;
    private final EntityManager entityManager;
    private final S3Service s3Service;

    @Transactional
    public User saveUser(User user, MultipartFile file) {
        var gender = genderRepository.findOrCreateByDescription(user.getGender().getDescription());
        var photo = s3Service.saveAndGetPhotos(file);
        user.setGender(gender);
        user.setPhoto(photo);
        photo.setUserId(user.getId());
        return userRepository.save(user);
    }

    @Transactional
    public void deleteUserById(Long id) {
        if (!userRepository.existsById(id)) {
            throw new CommonExistException(USER_DOES_NOT_EXIST);
        }
        userRepository.deleteById(id);
    }

    @Transactional
    public User getUserById(Long id) {
        return userRepository.findByIdAndDeletedIsFalse(id)
                .orElseThrow(() -> new CommonExistException(USER_DOES_NOT_EXIST));
    }

    @Transactional
    public User updateUserById(Long id, User user, MultipartFile file) {
        if (!userRepository.existsById(id)) {
            throw new CommonExistException(USER_DOES_NOT_EXIST);
        }
        return saveUser(user, file);
    }

    @Transactional
    public List<User> findAllUsers(boolean isDeleted) {
        Session session = entityManager.unwrap(Session.class);
        Filter filter = session.enableFilter("deletedUserFilter");
        filter.setParameter("isDeleted", isDeleted);
        Iterable<User> users =  userRepository.findAll();
        session.disableFilter("deletedUserFilter");
        return StreamSupport.stream(users.spliterator(), false)
                .collect(Collectors.toList());
    }
}
