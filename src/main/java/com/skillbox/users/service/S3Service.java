package com.skillbox.users.service;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.skillbox.users.entity.Photo;
import com.skillbox.users.repository.S3Repository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class S3Service {

    private final S3Repository s3Repository;


    public Photo saveAndGetPhotos(MultipartFile file) {
        var savedFile = saveFile(file);
        return getPhotoWithUrl(savedFile);
    }

    public void deletePhoto(String key) {
        s3Repository.delete(key);
    }

    private String saveFile(MultipartFile file) {
        try {
            var fileUuid = getUuid();
            s3Repository.put(fileUuid, file.getInputStream(), getObjectMetadata(file));
            return fileUuid;
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private Photo getPhotoWithUrl(String uuidFile) {
        var photo = new Photo();
        photo.setName(uuidFile);
        photo.setLink(s3Repository.getFileUrl(uuidFile));
        return photo;
    }

    private ObjectMetadata getObjectMetadata(MultipartFile file) {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(file.getSize());
        metadata.setContentType(MediaType.IMAGE_JPEG_VALUE);
        return metadata;
    }

    private String getUuid() {
        return UUID.randomUUID().toString();
    }
}
