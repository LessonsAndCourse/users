package com.skillbox.users.exceptions;

import io.micrometer.core.instrument.config.validate.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Component
@RequiredArgsConstructor
public class ExceptionConverter {

    public Error mapException(Exception ex) {
        if (ex instanceof ValidationException) {
            return mapException(BAD_REQUEST, ex);
        } else if (ex instanceof IllegalArgumentException || ex instanceof HttpMessageNotReadableException
                || ex instanceof TypeMismatchException || ex instanceof BindException
                || ex instanceof MissingServletRequestParameterException || ex instanceof MultipartException
                || ex instanceof MissingServletRequestPartException
        ) {
            return mapException(BAD_REQUEST, ex);
        } else if (ex instanceof CommonDataBaseException) {
            return mapException(HttpStatus.SERVICE_UNAVAILABLE, ex);
        } else {
            return mapException(HttpStatus.INTERNAL_SERVER_ERROR, ex);
        }
    }

    public Error mapException(HttpStatus status, Exception ex) {
        return new Error()
                .setStatus(status.value())
                .setDetail(ex.getMessage());
    }
}
