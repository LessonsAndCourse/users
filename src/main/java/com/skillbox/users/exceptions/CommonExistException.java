package com.skillbox.users.exceptions;

public class CommonExistException extends RuntimeException {
    public CommonExistException(String message) {
        super(message, new IllegalAccessError());
    }
}
