package com.skillbox.users.repository;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.skillbox.users.exceptions.CommonDataBaseException;
import com.skillbox.users.properties.S3Properties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.InputStream;

@Slf4j
@Service
@RequiredArgsConstructor
public class S3Repository {

    private static final String CANT_CREATE_BUCKET = "Can't create bucket";
    private static final String CANT_DELETE = "Can't delete file";
    private static final String CANT_PUT = "Can't put file";
    private static final String CANT_GET_FILE_URL = "Can't get file's URL";

    private final S3Properties properties;

    private final AmazonS3 s3Client;

    @PostConstruct
    private void createBucket() {
        try {
            String bucketName = properties.getBucketJpeg();
            if (!s3Client.doesBucketExistV2(bucketName)) {
                s3Client.createBucket(bucketName);
            }
        } catch (AmazonS3Exception ex) {
            log.error(CANT_CREATE_BUCKET);
            throw new CommonDataBaseException(CANT_CREATE_BUCKET, ex.getCause());
        }
    }

    public void delete(String key) {
        try {
            s3Client.deleteObject(properties.getBucketJpeg(), key);
        } catch (AmazonS3Exception ex) {
            log.error(CANT_DELETE);
            throw new CommonDataBaseException(CANT_DELETE, ex.getCause());
        }
    }

    public void put(String key, InputStream inputStream, ObjectMetadata metadata) {
        try {
            s3Client.putObject(properties.getBucketJpeg(), key, inputStream, metadata);
        } catch (AmazonS3Exception ex) {
            log.error(CANT_PUT);
            throw new CommonDataBaseException(CANT_PUT, ex.getCause());
        }
    }

    public String getFileUrl(String key) {
        try {
            GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(properties.getBucketJpeg(), key);
            return s3Client.generatePresignedUrl(urlRequest).toString();
        } catch (Exception ex) {
            log.error(CANT_GET_FILE_URL);
            throw new CommonDataBaseException(CANT_GET_FILE_URL, ex.getCause());
        }
    }
}
