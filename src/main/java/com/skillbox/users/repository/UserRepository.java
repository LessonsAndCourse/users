package com.skillbox.users.repository;

import com.skillbox.users.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByIdAndDeletedIsFalse(Long id);
}
