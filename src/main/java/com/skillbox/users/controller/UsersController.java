package com.skillbox.users.controller;

import com.skillbox.users.entity.User;
import com.skillbox.users.service.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UsersController {

    private final UsersService usersService;

    @PostMapping
    public User saveUser(@ModelAttribute("user") User user,
                         @RequestPart("file") MultipartFile file) {
        return usersService.saveUser(user, file);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUserById(@PathVariable Long id) {
        usersService.deleteUserById(id);
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable Long id) {
        return usersService.getUserById(id);
    }

    @GetMapping
    public List<User> findAllUsers(@RequestParam("isDeleted") Boolean isDeleted) {
        return usersService.findAllUsers(isDeleted);
    }

    @PostMapping("/{id}")
    public User updateUserById(@PathVariable Long id,
                               @ModelAttribute("user") User user,
                               @RequestPart("file") MultipartFile file) {
        return usersService.updateUserById(id, user, file);
    }
}
